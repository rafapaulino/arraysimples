﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySimples
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nome = new string[3];
            nome[0] = "Luana";
            nome[1] = "Mariana";
            nome[2] = "Daniele";

            Console.WriteLine(nome[0] +" "+ nome[1] +" "+ nome[2]);
            Console.WriteLine(String.Concat(nome));
            Console.WriteLine(nome.Length.ToString());
            Console.ReadKey();
        }
    }
}
